#include <cmn/mem.h>

cmn::mem cmn::copy_str(kcstr _str, dword _len)
{
	if(_str == null)
		return mem();

	kcstr str_tmp = _str;
	if(_len == 0)
	{
		while(*str_tmp++ != '\0');
		_len = (str_tmp - _str);
	}
	else
	{
		_len++;
	}

	mem str_mem = alloc_mem(_len);
	str_mem[_len-1] = '\0';

	for(dword i = 0; i < str_mem.length()-1; i++)
	{
		str_mem[i] = _str[i];
	}
	
	
	return str_mem;
}
cmn::mem cmn::alloc_mem(dword len)
{
	byte* ptr = new byte[len];

	if(!ptr)
	{
		len = 0;
	}
	 return mem(ptr, len);
}

cmn::mem::mem(byte* _ptr, dword _len)
{
	ptr = _ptr;
	len = _len;
}

bool	cmn::mem::is_vaild()
{
	return (ptr != 0);
}

void	cmn::mem::free()
{
	if(is_vaild())
	{
		delete ptr;
		ptr = 0;
		len = 0;
	}
}
cmn::dword cmn::mem::length() const
{
	return len;
}

cmn::mem::operator cmn::byte*() const
{
	return ptr;
}
