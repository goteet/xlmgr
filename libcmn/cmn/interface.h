#ifndef common_interface_h
#define common_interface_h

#include <cmn/typedef.h>
namespace cmn{
	class interface
	{
	public:
		virtual ~interface(){}
		virtual dword release() = 0;
	};

	class noncopy
	{
	public:
		noncopy(){}
	private:
		noncopy(const noncopy& rhs);
		noncopy& operator=(const noncopy&);
	};

	template<class type>
	class singleton : public noncopy
	{
	public:
		static type* instance()
		{static type inst;return &inst;}
	private:
		singleton();
	};
}
#endif
