#ifndef common_memory_h
#define common_memory_h

#include <cmn/typedef.h>
namespace cmn{
	class mem
	{
		byte*	ptr;
		dword	len;
	public:
		mem(byte* _ptr = 0, dword _len = 0);

		bool is_vaild();
		void free();
		dword length() const;
		operator byte*() const;
		
		template<class T>
		inline operator T*()  const { return reinterpret_cast<T*>(ptr); }
	};

	///this method is unsafe.
	///if _len==0, the method regard _str as ended by zero
	///otherwise, the method consider the length of _str is greater.
	mem copy_str(kcstr _str, dword _len = 0);
	mem alloc_mem(dword len);
}
#endif
