#ifndef common_type_define_h
#define common_type_define_h

namespace cmn{
	typedef signed char			int8;
	typedef signed short		int16;
	typedef signed int			int32;
	typedef signed long long	int64;

	typedef unsigned char		uint8;
	typedef unsigned short		uint16;
	typedef unsigned int		uint32;
	typedef unsigned long long	uint64;

	typedef uint8				byte;
	typedef uint16				word;
	typedef uint32				dword;
	typedef uint64				qword;
	typedef const uint8			kbyte;
	typedef const uint16		kword;
	typedef const uint32		kdword;
	typedef const uint64		kqword;

	typedef char*				cstr;
	typedef wchar_t*			cwstr;
	typedef const char*			kcstr;
	typedef const wchar_t*		kcwstr;

	#define null 0
	#define chr_eof 26
	template<class pointer>		inline void safe_release(pointer obj)	{if(obj){obj->release();obj=0;}}
	template<class pointer>		inline void safe_delete(pointer ptr)		{if(ptr){delete ptr;ptr=0;}}
	template<class array>		inline void safe_delete_arr(array arr)		{if(arr){delete []arr;arr=0;}}
}

#endif
