#pragma once
#include <sdk/function.h>
#include "xl_manager.h"

class add_plugin : public function
{
	virtual const char* name(){return "添加插件";}

	virtual void on_function();
};
/////////////////////////////////////////
class run_xl_platinum : public base_run_game
{
	virtual const char* name(){return "运行XL白金版";}

	virtual const char* exe_name(){return "CitiesXL_Platinum.exe";}
};

class run_xl_2011 : public base_run_game
{
	virtual const char* name(){return "运行XL 2011";}

	virtual const char* exe_name(){return "CitiesXL_2011.exe";}
};

class run_xl_2012 : public base_run_game
{
	virtual const char* name(){return "运行XL 2012";}

	virtual const char* exe_name(){return "CitiesXL_2012.exe";}
};