#pragma once

#include "list_entry.h"
#include <sdk/json.h>
	
#include <string>
using std::string;

class plugin : public list_entry
{
public:
	//property
	virtual char*	file()	{return (char*)m_plugin_data.file.c_str();}

	virtual char*	name()	{return (char*)m_plugin_data.name.c_str();}

	virtual bool	used()	{return m_plugin_data.used;}
	

	//operation
	virtual void switch_used()
	{
		m_plugin_data.used = !m_plugin_data.used;
		if(m_listener)m_listener->on_switch_used(this);
	}

	virtual void config()
	{
		if(m_listener)m_listener->on_config(this);
	}


	//others
	void set_used(bool _used){m_plugin_data.used = _used;}

	void set_name(const string& _name){m_plugin_data.name = _name;}

	plugin(const string& _file)
	{
		m_plugin_data.file = _file;
		m_listener = 0;
	}

	void set_listener(list_entry_listener* _l){ m_listener = _l;}

private:
	struct plugin_tok{
		std::string name;
		std::string file;
		bool used;
	};

	plugin_tok m_plugin_data;
	list_entry_listener* m_listener;
};