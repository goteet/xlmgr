#include "xl_manager.h"
#include "func_list.h"
#include <sdk/util.h>
#include <wingdi.h>

#pragma comment(lib, "plugin_manager_sdk.lib")

extern "C" __declspec(dllexport) manager* create_manager()
{
	g_manager = new xl_manager();
	return g_manager;
}

xl_manager::xl_manager()
{
	m_game_path = "";
	m_pak_path = "";
	m_bak_path = "";
}

void xl_manager::release()
{
	m_plugin_pool.destry();
	delete this;
}

const char* xl_manager::name(){return "XL 插件管理器";}
	
bool xl_manager::is_valid_game_path(const char* _path)
{
	return find_dir(_path, "paks");
}

void xl_manager::set_game_path(const char* _path)
{
	m_game_path = _path;

	m_pak_path = m_game_path + "\\paks";
	m_bak_path = m_game_path + "\\pak_bak";

	bool managed = find_dir(m_pak_path.c_str(), "manged");
	m_pak_path += "\\manged";

	if(!managed)
	{
		create_dir(m_pak_path.c_str());
	}

	if(!find_dir(m_game_path.c_str(), "pak_bak"))
	{
		create_dir(m_bak_path.c_str());
	}
}

const char* xl_manager::get_game_path()
{
	return m_game_path.c_str();
}

const char* xl_manager::get_pak_path()
{
	return m_pak_path.c_str();
}

const char* xl_manager::get_bak_path()
{
	return m_bak_path.c_str();
}

void xl_manager::save_config(jdoc& _doc)
{
	std::string game_path_utf8 = get_game_path();
	gbk_utf8(game_path_utf8);

	jval game_path;	
	game_path.SetString(game_path_utf8.c_str(), _doc.GetAllocator());
	_doc.AddMember("game_path", _doc.GetAllocator(), game_path, _doc.GetAllocator());
}

bool xl_manager::load_config(jdoc& _doc)
{
	if(!_doc.HasMember("game_path"))
		return false;

	jval& game_path = _doc["game_path"];
	std::string path = game_path.GetString();
	utf8_gbk(path);
	
	set_game_path(path.c_str());
	return true;
}

void xl_manager::save_file_list(jdoc& _doc)
{


}

bool xl_manager::load_file_list(jdoc& _doc)
{
	return true;
}

//functions
void xl_manager::get_list_functions(function**& _func_list, int& _cnt)
{
	int  i = 0;
	_cnt = 4;
	_func_list = new function*[_cnt];

#	define CREATE_PLUGIN(name) _func_list[i] = new name(); i++;
	CREATE_PLUGIN(add_plugin)
	CREATE_PLUGIN(run_xl_platinum)
	CREATE_PLUGIN(run_xl_2012)
	CREATE_PLUGIN(run_xl_2011)
#	undef CREATE_PLUGIN
}

void xl_manager::release_list_functions(function** _func_list, int _cnt)
{
	for(int i = 0; i < _cnt; i++)
	{
		if(_func_list[i]) delete _func_list[i];
	}
	delete []_func_list;
}

void xl_manager::get_plugin_name(const std::string& _path, std::string& new_path_)
{
	int blash_pos = _path.find_last_of(L'\\');
	int decimal_pos = _path.find_last_of(L'.');

	new_path_ = _path.substr(blash_pos + 1, decimal_pos-blash_pos-1);
}

void xl_manager::get_plugin_file(const std::string& _path, std::string& new_path_)
{
	int blash_pos = _path.find_last_of(L'\\');
	new_path_ = _path.substr(blash_pos + 1);
}

void xl_manager::on_install_plugin(std::string& _plugin_path)
{
	std::string plugin_name;
	std::string plugin_file;

	get_plugin_name(_plugin_path, plugin_name);
	get_plugin_file(_plugin_path, plugin_file);
	
	plugin* p = m_plugin_pool.get_plugin(plugin_name);
	if(!p)
	{
		plugin* entry = m_plugin_pool.install_plugin(plugin_name, plugin_file);
		if(entry)
		{
			entry->set_listener(this);
			m_list.add_entry(entry);
		}
		
		std::string patch_install_path = get_bak_path();
		patch_install_path = patch_install_path + "\\" + plugin_file;
		
		if(copy_file(_plugin_path.c_str(), patch_install_path.c_str()))
		{
			//get_context()->_ui->alert("安装成功");
		}
		else
		{
			//get_context()->_ui->alert("已经安装过此插件。将丢失的信息加入到列表中");
		}
	}
	else
	{
		string recover_patch_path;
		if(p->used())
		{
			recover_patch_path = get_pak_path();
			recover_patch_path = recover_patch_path + "\\" + plugin_file;
		}
		else
		{
			recover_patch_path = get_bak_path();
			recover_patch_path = recover_patch_path + "\\" + plugin_file;
		}

		if(copy_file(_plugin_path.c_str(), recover_patch_path.c_str()))
		{
			//get_context()->_ui->alert("列表中存在此插件，已经将丢失的patch复制到相应目录");
		}
		else
		{
			//get_context()->_ui->error("已经安装过此插件，安装停止");
		}
	}
	
}

//listener
void xl_manager::on_switch_used(list_entry* _p)
{
	if(_p->used())
	{
		load_plugin(_p->name());
	}
	else
	{
		unload_plugin(_p->name());
	}
}

void xl_manager::on_config(list_entry* _p)
{
	//get_context()->_ui->alert("haha");
}

bool xl_manager::on_read_file_list(list_entry* _p)
{
	plugin* p= (plugin*)_p;
	string path;
	bool exist = false;
	if(p->used())
	{
		exist = find_file(get_pak_path(), p->file());
	}
	else
	{
		exist = find_file(get_bak_path(), p->file());
	}
	if(exist)
	{
		p->set_listener(this);
		m_list.add_entry(p);

		return true;
	}

	//get_context()->_ui->alert("有插件丢失，将其从列表中删除");
	return false;
}

void xl_manager::load_plugin(const string& _plugin_name)
{
	
	plugin* p = m_plugin_pool.get_plugin(_plugin_name);
	if(p)
	{
		string install_patch_path = get_pak_path();
		string backup_patch_path = get_bak_path();
		install_patch_path = install_patch_path+ "\\" + p->file();
		backup_patch_path = backup_patch_path+ "\\" + p->file();
		force_move_file(backup_patch_path.c_str(), install_patch_path.c_str());

		m_plugin_pool.load_plugin(_plugin_name);
	}
}

void xl_manager::unload_plugin(const string& _plugin_name)
{
	
	plugin* p = m_plugin_pool.get_plugin(_plugin_name);
	if(p)
	{
		string install_patch_path = get_pak_path();
		string backup_patch_path = get_bak_path();
		install_patch_path = install_patch_path + "\\" + p->file();
		backup_patch_path = backup_patch_path + "\\" + p->file();

		force_move_file(install_patch_path.c_str(), backup_patch_path.c_str());
		m_plugin_pool.unload_plugin(_plugin_name);
	}
}
