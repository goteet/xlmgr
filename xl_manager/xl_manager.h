#pragma once

#include <sdk/manager.h>
#include <string>
#include "listview_xl.h"
#include "plugin_pool.h"
class xl_manager : public manager, public list_entry_listener
{
public:
	xl_manager();

	//game path
	virtual void release();

	virtual const char* name();

	virtual bool is_valid_game_path(const char* _path);

	virtual void set_game_path(const char* _path);

	virtual const char* get_game_path();

	virtual const char* get_pak_path();

	virtual const char* get_bak_path();

	//configuration
	virtual void save_config(jdoc& _doc);

	virtual bool load_config(jdoc& _doc);

	virtual void save_file_list(jdoc& _doc);

	virtual bool load_file_list(jdoc& _doc);

	//list view
	virtual listview* get_list_ctrl(){ return &m_list; }

	//functions
	virtual void get_list_functions(function**&, int& cnt);

	virtual void release_list_functions(function** _func_list, int _cnt);

	virtual void on_switch_used(list_entry* _p);

	virtual void on_config(list_entry* _p);

	virtual bool on_read_file_list(list_entry* _p);

public:
	void on_install_plugin(std::string& _path);

private:
	void get_plugin_name(const std::string& _path, std::string& new_path_);
	
	void get_plugin_file(const std::string& _path, std::string& new_path_);

	void load_plugin(const string& _plugin_name);

	void unload_plugin(const string& _plugin_name);


	std::string m_game_path;
	std::string m_pak_path;
	std::string m_bak_path;

	plugin_pool m_plugin_pool;

	listview_xl m_list;
};

inline xl_manager* get_mgr()
{ return (xl_manager*)g_manager;}