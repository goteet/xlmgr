#pragma once
#include <afxcmn.h>
#include <sdk/manager.h>
#include "list_entry.h"
#include <map>

class listview_xl : public listview
{
public:
	virtual ~listview_xl(){}

	//void add_entry(list_entry* _entry);

	virtual bool initial();

	virtual void deinitial();

	virtual bool on_click_item(int _rid, int _cid);

	virtual bool on_double_click_item(int _rid, int _cid);

	virtual list_column& get_column(int _index);

	virtual bool on_column_check(int _rid, int _cid);

	virtual char* on_column_text(int _rid, int _cid);

	virtual void on_color_column_text(int _rid, int _cid, DWORD& _color){_color = RGB(50,50,50);}

	virtual void on_color_column_bk(int _rid, DWORD& _color){_color = RGB(250,200,250);}

	void add_entry(list_entry* _entry);
private:
	void create_columns_head();

	list_entry* get_entry(int _index);

	void switch_entry_used(int _index);

	typedef std::map<int, list_entry*> entry_pool;
	typedef entry_pool::iterator entry_pool_iter;
	entry_pool m_entrys;

	enum{ col_title=0, col_desc=1, col_used=2 };
	enum{ m_column_cnt = 3};
	list_column m_columns[m_column_cnt];
};


