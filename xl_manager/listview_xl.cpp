// listview_xl.cpp : implementation file
//
#include "listview_xl.h"

bool listview_xl::initial()
{
	create_columns_head();
	return true;
}

void listview_xl::deinitial()
{
	for(int i = 0; i < m_column_cnt; i++)
	{
		m_columns[i].title.free();
	}
}

list_column& listview_xl::get_column(int _index)
{
	return m_columns[_index];
}

void listview_xl::add_entry(list_entry* _entry)
{
	int index = GetItemCount();
	m_entrys[index] = _entry;

		LVITEM lvI;

    // Initialize LVITEM members that are common to all items.
    lvI.pszText   = LPSTR_TEXTCALLBACK; // Sends an LVN_GETDISPINFO message.
	lvI.mask      = LVIF_TEXT;
    lvI.stateMask = 0;
	lvI.state     = 0;
	lvI.iSubItem  = 0;

	lvI.iItem = index;
	InsertItem(&lvI);
}

void listview_xl::create_columns_head()
{
	LVCOLUMN lvc;
	lvc.mask = LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	m_columns[col_title].title = cmn::copy_str("名称");
	m_columns[col_desc].title = cmn::copy_str("文件");
	m_columns[col_used].title = cmn::copy_str("使用");

	m_columns[col_title].width = 140;
	m_columns[col_desc].width = 220;
	m_columns[col_used].width = 40;

	m_columns[col_title].ctype = ct_text;
	m_columns[col_desc].ctype = ct_text;
	m_columns[col_used].ctype = ct_check;

	m_columns[col_title].is_fix = false;
	m_columns[col_desc].is_fix = false;
	m_columns[col_used].is_fix = true;

	for(int i = 0; i < m_column_cnt; i++)
	{
		lvc.pszText = m_columns[i].title;
		lvc.cx = m_columns[i].width;
		lvc.iSubItem = i;
		InsertColumn(i, &lvc);
	}
}

list_entry* listview_xl::get_entry(int _index)
{
	entry_pool_iter ifind = m_entrys.find(_index);
	if(ifind != m_entrys.end())
	{
		return ifind->second;
	}
	return 0;
}

void listview_xl::switch_entry_used(int _index)
{
	//list_entry* entry = get_entry(_index);
	//if(entry)
	{
		//entry->swich_check();
		InvalidateRect(NULL);
	}
}

bool listview_xl::on_column_check(int _rid, int _cid)
{
	list_entry* p = get_entry(_rid);
	if(p)
	{
		if(_cid == col_used)
		{
			return p->used();
		}
	}
	return false;
}

char* listview_xl::on_column_text(int _rid, int _cid)
{
	list_entry* p = get_entry(_rid);
	if(p)
	{
		if(_cid == col_title)
		{
			return p->name();
		}
		else if(_cid == col_desc)
		{
			return p->file();
		}
	}
	return "";
}

bool listview_xl::on_click_item(int _rid, int _cid)
{
	list_entry* p = get_entry(_rid);
	if(p)
	{
		if(_cid == col_used)
		{
			p->switch_used();
			return true;
		}
	}
	return false;
}

bool listview_xl::on_double_click_item(int _rid, int _cid)
{
	list_entry* p = get_entry(_rid);
	if(p)
	{
		if(_cid == col_title || _cid == col_desc)
		{
			p->config();
			return true;
		}
	}
	return false;
}
