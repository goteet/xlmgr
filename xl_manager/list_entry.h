#pragma once
class list_entry
{
public:
	virtual ~list_entry(){}

	//property
	virtual char* file() = 0;

	virtual char* name() = 0;

	virtual bool used() = 0;

	//operation
	virtual void switch_used() = 0;

	virtual void config() = 0;
};

class list_entry_listener
{
public:
	virtual ~list_entry_listener(){}

	virtual void on_switch_used(list_entry* _p) = 0;

	virtual void on_config(list_entry* _p) = 0;

	virtual bool on_read_file_list(list_entry* _p) = 0;
};