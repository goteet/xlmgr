#include "func_list.h"
#include <sdk/manager.h>
#include <string>
#include "afxdlgs.h"
void add_plugin::on_function()
{
	CFileDialog fd(TRUE);
	OPENFILENAME& ofn = fd.GetOFN();

	const int max_file_cnt = 32;
	const int max_filename_cnt = 32 * MAX_PATH + 1;
	char filename_buffer[max_filename_cnt];
	memset(filename_buffer, 0, max_filename_cnt);

	char* fitlers =
		"patch\0*.patch\0";
	"all files\0*.*\0\0";

	ofn.lpstrFile = filename_buffer;
	ofn.nMaxFile  = max_filename_cnt;
	ofn.lpstrFilter = fitlers;

	if(fd.DoModal() == IDOK)
	{
		std::string plugin_path = filename_buffer;
		get_mgr()->on_install_plugin(plugin_path);
	}
}