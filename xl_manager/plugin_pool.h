#pragma once

#include "plugin.h"
#include <map>
using std::map;

class plugin_pool
{
public:
	plugin* install_plugin(const string& _name,const string& _file, bool _used = true);

	void load_plugin(const string& _name);

	void unload_plugin(const string& _name);

	plugin* get_plugin(const string& _name);

	void read_json(jval& _root, list_entry_listener* _l);

	void write_json(jdoc& _doc, jval& _parent);

	void destry();

private:
	typedef map<string, plugin*> plugin_map;
	typedef plugin_map::iterator plugin_map_iter;

	plugin_map m_plugins;
};
