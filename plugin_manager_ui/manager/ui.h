#pragma once

#include <string>
using std::string;

class CEdit;
class CButton;
class listview_plugin;
class list_entry;

class ui
{
public:
	virtual void error(string) = 0;

	virtual void alert(string) = 0;

	virtual void set_game_path(const char*) = 0;

	virtual void initial_functions() = 0;

	virtual void release_functions() = 0;

	virtual void add_list_entry(list_entry* _entry) = 0;
};

struct context{
	ui*			_ui;
};

extern context* get_context();