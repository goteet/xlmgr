
// xl_plugin_manager.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "../app.h"

#include "ui.h"

#include <sdk/json.h>
#include <algorithm>
#include <sdk/util.h>
#pragma comment(lib, "plugin_manager_sdk.lib")

context* get_context()
{
	static context g_context;
	return &g_context;
}

bool app::load_xl_plugin()
{
	HMODULE h_plugin = ::LoadLibrary("xl_manager.dll");
	if(h_plugin)
	{
		manager_creator creator = (manager_creator)::GetProcAddress(h_plugin, "create_manager");
		if(creator)
		{
			m_manager = creator();
			return true;
		}

		::FreeLibrary(h_plugin);
	}
	return false;
}

bool app::initial()
{
	if(!load_xl_plugin())
		return false;
	
	g_manager = m_manager;

	return true;
}

bool app::init_dlg()
{
	if(!read_config())
		return false;

	return true;
}

void app::deinitial()
{
	if(m_manager->get_game_path() != "")
	{
		write_config();
	}

	get_context()->_ui->release_functions();

	//m_plugin_pool.destry();

	if(m_manager)
	{
		m_manager->release();
		m_manager = 0;
	}
}

bool app::read_config()
{
	jdoc config;
	bool is_valid = true;
	char* json = get_file_json(DEF_NAME_CFG);

	if(!json)
	{
		is_valid = false;
		goto return_lab;
	}

	
	if(!config.ParseInsitu<0>(json).HasParseError())
	{
		if(!m_manager->load_config(config)
			|| !m_manager->is_valid_game_path(m_manager->get_game_path()) )
		{
			get_context()->_ui->error("游戏目录下找不到插件目录：Paks，请重新指定。");
			is_valid = false;
			goto return_lab;
		}

		if(!m_manager->load_file_list(config))
		{
			is_valid = false;
			goto return_lab;
		}

		//update UI.
		set_game_path(m_manager->get_game_path());
		jval& plugin_list = config["plugin_list"];
//		m_plugin_pool.read_json(plugin_list, this);
	}

return_lab:
	if(json)delete []json;
	return is_valid;
}

void app::write_config()
{
	jdoc config;
	if(config.ParseInsitu<0>("{}").HasParseError())
	{
		return;
	}

	m_manager->save_config(config);
	m_manager->save_file_list(config);

	

	jval plugin_list;
	plugin_list.SetObject();
//	m_plugin_pool.write_json(config, plugin_list);

	config.AddMember("plugin_list", config.GetAllocator(), plugin_list, config.GetAllocator());

	FILE* file = fopen(DEF_NAME_CFG, "wb");

	jfstream f(file);
	jfwriter writer(f);

	config.Accept(writer);
}

void app::set_game_path(const string& _game_path)
{
	
	if(!m_manager->is_valid_game_path(_game_path.c_str()))
	{
		get_context()->_ui->error("游戏目录下找不到插件目录：Paks，请重新指定。");
	}
	else
	{
		m_manager->set_game_path(_game_path.c_str());
		get_context()->_ui->set_game_path(_game_path.c_str());
		get_context()->_ui->initial_functions();
	}
}

char* app::get_file_json(const char* _file)
{
	FILE* file = fopen(_file, "rb");
	if(file)
	{
		fseek(file, 0, SEEK_END);
		unsigned file_length = ftell(file);
		char* file_buffer = new char[file_length+1];
		
		fseek(file, 0, SEEK_SET);
		fread(file_buffer, 1, file_length, file);
		fclose(file);

		file_buffer[file_length] = '\0';
		return file_buffer;
	}
	return 0;
}