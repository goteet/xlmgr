
// main_dlg.h : ͷ�ļ�
//

#pragma once
#include "afxwin.h"
#include "manager/ui.h"
#include "Resource.h"
#include <sdk/function.h>

class dlg_main : public CDialogEx, public ui
{
public:
	dlg_main(CWnd* pParent = NULL);	

	enum { IDD = IDD_MAIN };

	void deinitial();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	
	virtual BOOL OnInitDialog();

	virtual void set_game_path(const char* _path);

	virtual void error(string _log);

	virtual void alert(string _log);

	virtual void initial_functions();

	virtual void release_functions();

	virtual void add_list_entry(list_entry* _entry);

	void adjust_layout();

	DECLARE_MESSAGE_MAP()

private:
	HICON m_hIcon;

	CEdit m_edit_game_path;
	CButton m_btn_load_plugin;

	enum{FUNC_BTNID_BEG = WM_USER+0x100, FUNC_BTNID_END = FUNC_BTNID_BEG+ 0x1FF};
	CMFCButton**m_func_btn;
	function**	m_func_list;
	int			m_func_cnt;

public:
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void on_browse_game_path();
	afx_msg void on_func_btn(UINT _id);
};
