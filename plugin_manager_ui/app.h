
// xl_plugin_manager.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include <sdk/json.h>
#include "resource.h"		// 主符号

#include <string>
using std::string;

#include "manager/ui.h"
#include <sdk/manager.h>

class app : public CWinApp
{
public:
	virtual BOOL InitInstance();

public:
	void set_game_path(const string& _game_path);

	bool initial();

	bool init_dlg();

	void deinitial();

private:
	bool load_xl_plugin();

	bool read_config();

	void write_config();

	char* get_file_json(const char* _file);

	manager* m_manager;

	DECLARE_MESSAGE_MAP()
};

extern app theApp;