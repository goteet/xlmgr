//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xl_plugin_manager.rc
//
#define IDD_MAIN                        102
#define IDI_LOGO                        103
#define IDC_EDIT_GAME_PATH              1000
#define IDC_BTN_GAME_PATH               1001
#define IDC_BTN_EXPORT_ZIP              1002
#define IDC_LIST_PLUGIN                 1004
#define IDC_BTN_START_GAME              1004
#define IDC_PLUGIN_LIST_AREA            1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
