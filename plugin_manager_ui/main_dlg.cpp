
// main_dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "app.h"
#include "main_dlg.h"
#include "afxdialogex.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


namespace{
	const int k_win_border	= 10;
	const int k_win_title	= 40;
	const int k_ctrl_margin = 20;

	const int k_btn_w = 100;
	const int k_btn_h = 25;

	const int k_path_w = 350;

	const int k_list_w = k_path_w + k_btn_w + k_ctrl_margin;
	const int k_list_h = 300;

	const int k_win_w = k_list_w + 2 * k_ctrl_margin + k_win_border * 2;
	const int k_win_h = k_list_h + 3 * k_ctrl_margin + k_btn_h + k_win_title + k_win_border;

	const int k_func_list_left = 2 * k_ctrl_margin + k_list_w;
	const int k_func_list_h = (k_win_h - 2 * k_ctrl_margin) / (k_btn_h + k_ctrl_margin);
}


bool open_folder_browser(string wndTitle, string& path_, HWND pParent = NULL)
{
    char        szDir[MAX_PATH] = {0};
    BROWSEINFO    bi;
    ITEMIDLIST    *pidl = NULL;

    bi.hwndOwner = pParent;
    bi.pidlRoot = NULL;
    bi.pszDisplayName = szDir;
    bi.lpszTitle = wndTitle.c_str();
    bi.ulFlags = BIF_RETURNONLYFSDIRS;
    bi.lpfn = NULL;
    bi.lParam = 0;
    bi.iImage = 0;

    pidl = SHBrowseForFolder(&bi);
    if(NULL == pidl || !SHGetPathFromIDList(pidl,szDir))
	{
		return false;
	}
    path_ = szDir;
	return true;
}

dlg_main::dlg_main(CWnd* pParent /*=NULL*/)
	: CDialogEx(dlg_main::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_LOGO);

	m_func_list = 0;
	m_func_cnt = 0;
}

void dlg_main::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_GAME_PATH, m_edit_game_path);
}

void dlg_main::on_browse_game_path()
{
	// TODO: Add your control notification handler code here
	string game_path;
	if(open_folder_browser("请选择游戏目录", game_path, GetSafeHwnd()))
	{
		theApp.set_game_path(game_path);
	}
}

void dlg_main::set_game_path(const char* _path)
{
	m_edit_game_path.SetWindowText(_path);
	adjust_layout();
}

void dlg_main::error(string _log)
{
	AfxMessageBox(_log.c_str(), MB_ICONSTOP);
}

void dlg_main::alert(string _log)
{
	AfxMessageBox(_log.c_str(), MB_ICONINFORMATION);
}

void dlg_main::add_list_entry(list_entry* _entry)
{
}

void dlg_main::initial_functions()
{
	release_functions();

	g_manager->get_list_functions(m_func_list, m_func_cnt);
	m_func_btn = new CMFCButton*[m_func_cnt];
	for(int i = 0; i < m_func_cnt; i++)
	{
		CRect r;
		m_func_btn[i] = new CMFCButton();
		if(m_func_list[i])
		{
			m_func_btn[i]->Create(
				m_func_list[i]->name(),
				WS_CHILD | WS_VISIBLE, 
				r, this,
				FUNC_BTNID_BEG+i);

		}
		else
		{
			m_func_btn[i]->Create("功能错误", 	WS_CHILD | WS_VISIBLE, r, this, FUNC_BTNID_END+1);
			m_func_btn[i]->EnableWindow(FALSE);
		}

			 
	}
	adjust_layout();
}

void dlg_main::release_functions()
{
	if(!m_func_list || !m_func_cnt)
		return;

	g_manager->release_list_functions(m_func_list, m_func_cnt);
	
	for(int i = 0; i < m_func_cnt; i++)
	{
		delete m_func_btn[i];
		m_func_btn[i] = NULL;
	}
	delete []m_func_btn;
	
	
	m_func_list = 0;
	m_func_cnt = 0;
}

BEGIN_MESSAGE_MAP(dlg_main, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_GAME_PATH, &dlg_main::on_browse_game_path)
	ON_COMMAND_RANGE(FUNC_BTNID_BEG, FUNC_BTNID_END, on_func_btn)
END_MESSAGE_MAP()

// dlg_main 消息处理程序

BOOL dlg_main::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetIcon(m_hIcon, FALSE);
	SetIcon(m_hIcon, TRUE);
	
	
	
	CRect rect;
	rect.left = k_ctrl_margin;
	rect.right = rect.left + k_list_w;
	rect.top = 2 * k_ctrl_margin + k_btn_h;
	rect.bottom = rect.top + k_list_h;

	g_manager->get_list_ctrl()->create(rect, this, IDC_LIST_PLUGIN);

	if(!theApp.init_dlg())
		return FALSE;

	std::string  title = g_manager->name();
	SetWindowText( title.c_str() );

	adjust_layout();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void dlg_main::adjust_layout()
{
	const int func_col = m_func_cnt / k_func_list_h + 1;
	const int func_row = m_func_cnt % k_func_list_h;

	CRect rect;
	
	rect.left = 0;
	rect.top = 0;
	rect.right = rect.left + k_win_w;
	rect.bottom = rect.top + k_win_h;

	rect.right += (k_ctrl_margin + k_btn_w) * func_col;

	ClientToScreen(rect);
	MoveWindow(rect);


	rect.left = k_ctrl_margin;
	rect.right = rect.left + k_path_w;
	rect.top = k_ctrl_margin;
	rect.bottom = rect.top + k_btn_h;
	GetDlgItem(IDC_EDIT_GAME_PATH)->MoveWindow(rect);

	rect.left = rect.right + k_ctrl_margin;
	rect.right = rect.left + k_btn_w;
	GetDlgItem(IDC_BTN_GAME_PATH)->MoveWindow(rect);

	rect.left = k_ctrl_margin;
	rect.right = rect.left + k_list_w;
	rect.top = 2 * k_ctrl_margin + k_btn_h;
	rect.bottom = rect.top + k_list_h;

	g_manager->get_list_ctrl()->MoveWindow(rect);

	//test code
	//return;

	for(int c = 0; c < func_col; c++)
	{
		for(int i = 0; i < func_row; i++)
		{
			CRect r;
			r.left = k_func_list_left + c * (k_ctrl_margin + k_btn_w);
			r.right = r.left + k_btn_w;

			r.top = k_ctrl_margin + (k_btn_h+k_ctrl_margin)*i;
			r.bottom = r.top + k_btn_h;
			m_func_btn[i]->MoveWindow(r);
		}
	}
}

void dlg_main::deinitial()
{
	g_manager->get_list_ctrl()->deinitial();
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。
void dlg_main::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR dlg_main::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void dlg_main::on_func_btn(UINT _id)
{
	int func_id = _id - FUNC_BTNID_BEG;
	if(m_func_list[func_id]) m_func_list[func_id]->on_function();
}
