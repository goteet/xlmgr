#include <string>
#include <sdk/manager.h>

manager* g_manager;

void base_run_game::on_function()
{
	std::string game_exe_path = g_manager->get_game_path();
	game_exe_path = game_exe_path + "\\" + exe_name();
	PROCESS_INFORMATION pi;
	STARTUPINFO si = {sizeof(si)};
	// 启动进程
	BOOL ret = CreateProcess(NULL, (LPSTR)game_exe_path.c_str(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	if (ret){
		// 关闭子进程的主线程句柄
		CloseHandle(pi.hThread);
		// 关闭子进程句柄
		CloseHandle(pi.hProcess);
	}
}