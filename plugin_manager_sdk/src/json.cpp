#include <sdk/json.h>
#include <iconv/iconv.h>
#pragma comment(lib, "libiconv.lib")
#include <stdio.h>

const char*  DEF_NAME_CFG = ".\\config";
const char*  DEF_NAME_FILE_LIST = ".\\config";

void gbk_utf8(std::string &_gbk_)
{
	iconv_t iconvH;
	iconvH = iconv_open("utf-8","gbk");

	if(iconvH == 0)
	{
		return ;
	}

	const char* strChar = _gbk_.c_str();
	const char** pin = &strChar;

	size_t strLength = _gbk_.length();
	size_t outLength = strLength/4;
	char* outbuf = new char[outLength];
	char* pBuff = outbuf;
	memset(outbuf,0,outLength);

	if(-1 == iconv(iconvH,
		pin,
		&strLength,
		&outbuf,
		&outLength))
	{
		delete []pBuff;
		iconv_close(iconvH);
		return ;
	}

	_gbk_ = pBuff;
	delete []pBuff;
	iconv_close(iconvH);
}

void utf8_gbk(std::string &_utf8_)
{
	iconv_t iconvH;
	iconvH = iconv_open("gbk", "utf-8");

	if(iconvH == 0)
	{
		return ;
	}

	const char* strChar = _utf8_.c_str();
	const char** pin = &strChar;

	size_t strLength = _utf8_.length();
	size_t outLength = strLength*4;
	char* outbuf = new char[outLength];
	char* pBuff = outbuf;
	memset(outbuf,0,outLength);

	if(-1 == iconv(iconvH,
		pin,
		&strLength,
		&outbuf,
		&outLength))
	{
		delete []pBuff;
		iconv_close(iconvH);
		return ;
	}

	_utf8_ = pBuff;
	delete []pBuff;
	iconv_close(iconvH);
}