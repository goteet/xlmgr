#pragma once

#include <rapidjson/document.h>		// rapidjson's DOM-style API
#include <rapidjson/prettywriter.h>	// for stringify JSON
#include <rapidjson/filestream.h>	// wrapper of C stream for prettywriter as output

#include <string>

typedef rapidjson::Document  jdoc;

typedef rapidjson::Value jval;

typedef rapidjson::FileStream jfstream;

typedef rapidjson::PrettyWriter<rapidjson::FileStream> jfwriter;

typedef jval::Member jmember;

void gbk_utf8(std::string &_gbk_);

void utf8_gbk(std::string &_utf8_);

extern const char* DEF_NAME_CFG;

extern const char* DEF_NAME_FILE_LIST;