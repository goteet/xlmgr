#pragma once

bool find_dir(const char* _path, const char* _dir);

bool find_file(const char* _path, const char* _file);

bool create_dir(const char* _path);

bool force_move_file(const char* _src_file, const char* _dst_file);

bool copy_file(const char* _src_file, const char* _dst_file);