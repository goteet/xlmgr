#pragma once

class function
{
public:
	virtual ~function(){}

	virtual const char* name() = 0;

	virtual void on_function() = 0;
};

class base_run_game : public function
{
	virtual const char* name() = 0;

	virtual const char* exe_name() = 0;

	virtual void on_function();
};