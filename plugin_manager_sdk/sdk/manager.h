#pragma once 

#include <afxwin.h> //保证该语句在首行
#include <afxcmn.h>
#include <sdk/json.h>
#include <cmn/mem.h>
#include <sdk/function.h>


using cmn::mem;
enum{ ct_text, ct_check };
struct list_column{
	mem		title;
	int		width;
	int		ctype;
	bool	is_fix;	
	
};

class listview : public CListCtrl
{
public:
	virtual bool initial() = 0;

	virtual void deinitial() = 0;

private:
	virtual bool on_click_item(int _rid, int _cid) = 0;

	virtual bool on_double_click_item(int _rid, int _cid) = 0;

	virtual bool on_column_check(int _rid, int _cid) = 0;

	virtual char* on_column_text(int _rid, int _cid) = 0;

	virtual void on_color_column_text(int _rid, int _cid, DWORD& _color) = 0;

	virtual void on_color_column_bk(int _rid, DWORD& _color) = 0;

	virtual list_column& get_column(int _cid) = 0;

public:
	virtual ~listview(){}

	void create(CRect&, CWnd* _parent, UINT _msg_id);

private:
	virtual void PreSubclassWindow();

	bool draw_subitem(LPNMLVCUSTOMDRAW);

	bool double_click_item(LPNMITEMACTIVATE);
	
	void click_item(LPNMITEMACTIVATE);

	DECLARE_DYNAMIC(listview)
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchanging(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnGetdispinfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemActivate(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
};

class manager
{
public:
	virtual void release() = 0;

	virtual const char* name() = 0;

	//game path
	virtual bool is_valid_game_path(const char* _path) = 0;

	virtual void set_game_path(const char* _path) = 0;

	virtual const char* get_game_path() = 0;

	virtual const char* get_pak_path() = 0;

	virtual const char* get_bak_path() = 0;

	//configuration
	virtual void save_config(jdoc& _doc) = 0;

	virtual bool load_config(jdoc& _doc) = 0;

	virtual void save_file_list(jdoc& _doc) = 0;

	virtual bool load_file_list(jdoc& _doc) = 0;

	//list view
	virtual listview* get_list_ctrl() = 0;

	//functions
	virtual void get_list_functions(function**&, int& cnt) = 0;

	virtual void release_list_functions(function**, int cnt) = 0;
};

typedef manager* (*manager_creator)();
extern manager* g_manager;